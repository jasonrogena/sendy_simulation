package test.sendyit.com.sendyandroidtask.handlers;

import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import test.sendyit.com.sendyandroidtask.R;

/**
 * This class carries information on a rider. It also handles all events that are rider specific
 *
 * Created by Jason Rogena: jasonrogena@gmail.com
 */
public class Rider {

    private static final String TAG = "Sendy.Rider";
    private static final String KEY_NAME = "name";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_ID = "rider_id";
    private static final String KEY_ONDELIVERY = "ondelivery";
    private static final String KEY_POSITIONS = "positions";

    private final ArrayList<Point> points;
    private final String name;
    private final String phone;
    private final int id;
    private final boolean ondeliver;
    private final Marker marker;
    private final Polyline polyline;
    private final List<LatLng> drawnLine;
    private final GoogleMap googleMap;
    private int lastViewedPointIndex;
    private long calibrationTime;

    public Rider(GoogleMap googleMap, JSONObject riderObject) throws JSONException{
        this.calibrationTime = -1;
        this.lastViewedPointIndex = 0;
        this.name = riderObject.getString(KEY_NAME);
        this.phone = riderObject.getString(KEY_PHONE);
        this.id = riderObject.getInt(KEY_ID);
        this.ondeliver = riderObject.getBoolean(KEY_ONDELIVERY);
        this.points = new ArrayList<Point>();
        setPoints(riderObject.getJSONArray(KEY_POSITIONS));
        this.googleMap = googleMap;
        this.drawnLine = new ArrayList<LatLng>();
        this.polyline = initPolyline();
        this.marker = initMarker();
    }

    /**
     * Initializes the marker on the map corresponding to the current rider
     *
     * @return The marker object
     * @see com.google.android.gms.maps.model.Marker
     */
    private Marker initMarker() {

        if(this.points.size() > 0 && this.googleMap != null) {
            Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time
            Marker marker = googleMap.addMarker(new MarkerOptions().position(this.points.get(lastViewedPointIndex).getLatLng())
                    .title(this.name)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_east)).alpha(0.7f));
            marker.setVisible(false);//only unhide the marker when the rider starts moving
            return marker;
        }
        return null;
    }

    /**
     * Returns the time for the first point corresponding to this rider
     *
     * @return The Date object
     * @see java.util.Date
     */
    public long getStartTime() {
        Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time
        return this.points.get(0).getTime().getTime();
    }

    /**
     * Returns the time for the last point corresponding to this rider
     *
     * @return The Date object
     * @see java.util.Date
     */
    public long getFinalTime() {
        Collections.sort(this.points, new Point.PointComparator());//make sure you sort the points ordered based on time
        return this.points.get(this.points.size() - 1).getTime().getTime();
    }

    /**
     * Sets the time difference (in milliseconds) between now and the actual time in which the location
     * data was captured
     *
     * @param calibrationTime   The time difference in milliseconds
     */
    public void setCalibrationTime(long calibrationTime) {
        this.calibrationTime = calibrationTime;
    }

    /**
     * Returns the location of the rider's marker on the map
     *
     * @return The LatLng object representing the current location
     * @see com.google.android.gms.maps.model.LatLng
     */
    public LatLng getCurrentLocation() {
        return this.points.get(lastViewedPointIndex).getLatLng();
    }

    /**
     * Returns the time difference between when the simulation was started and when the data was
     * actually collected
     *
     * @return The time difference in milliseconds
     */
    public long getCalibrationTime() {
        return this.calibrationTime;
    }

    /**
     * Shows the rider's position and polyline reprepresenting his/her movement on the map
     */
    public void show() {
        this.marker.setVisible(true);
        this.polyline.setVisible(true);
    }

    /**
     * Hides the rider's position and polyline representing his/her movement on the map
     */
    public void hide() {
        this.marker.setVisible(false);
        this.polyline.setVisible(false);
    }

    /**
     * Initializes the polyline representing the riders movement on the map
     *
     * @return The Polyline object
     * @see com.google.android.gms.maps.model.Polyline
     */
    private Polyline initPolyline() {
        if(this.googleMap != null && this.points.size() > 0 && this.drawnLine != null) {
            this.drawnLine.add(this.points.get(lastViewedPointIndex).getLatLng());
            Polyline polyline = googleMap.addPolyline(new PolylineOptions().addAll(drawnLine).width(10).color(getRandomColor()));
            polyline.setVisible(false);//only unhide when the rider starts moving
            return polyline;
        }
        return null;
    }

    /**
     * Returns a random dark color
     *
     * @return A Color object
     * @see android.graphics.Color
     */
    private int getRandomColor() {
        int r = 100 + (int)(Math.random()*255);
        int g = 100 + (int)(Math.random()*255);
        int b = 100 + (int)(Math.random()*255);

        return  Color.rgb(r, g, b);
    }

    /**
     * Initializes the points corresponding to this rider from the provided JSONArray
     *
     * @param pointArray    The JSONArray carrying the rider's points
     * @see org.json.JSONArray
     * @see test.sendyit.com.sendyandroidtask.handlers.Point
     */
    public void setPoints(JSONArray pointArray) {
        for(int index = 0; index < pointArray.length(); index++) {
            try {
                this.points.add(new Point(pointArray.getJSONObject(index)));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "A JSON error occurred while trying to initialize a point in the points");
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, "A parsing error occurred while trying to initialize a point in the points");
            }
        }
    }

    /**
     * Animates the rider's position on the map to the current simulated time
     */
    public void animateToTime() {
        Date time = new Date();
        Collections.sort(points, new Point.PointComparator());//make sure that the points are ordered based on time
        for(int index = lastViewedPointIndex + 1; index < points.size(); index++) {
            //move the marker
            if(index > 0 && (calibrationTime + points.get(index).getTime().getTime()) <= time.getTime()) {
                long timeDiff = points.get(index).getTime().getTime() - points.get(index - 1).getTime().getTime();
                boolean hide = false;
                if(index == points.size() - 1) hide = true;
                moveMarker(points.get(index).getLatLng(), 100, hide);
                lastViewedPointIndex = index;
            }
        }
    }

    /**
     * Performs the actual animation of moving the marker and polyline on the map
     *
     * @param toLatLng  The position on the map to send the marker
     * @param duration  The duration to take for the movement
     * @param hide      Whether to hide the rider after the animation
     * @see com.google.android.gms.maps.model.LatLng
     */
    private void moveMarker(final LatLng toLatLng, final long duration, final boolean hide) {
        show();
        Log.d(TAG, "Moving marker to "+toLatLng.toString()+" in "+duration+ " for "+this.name);
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection projection = googleMap.getProjection();
        android.graphics.Point startPoint = projection.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final LinearInterpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                if((toLatLng.longitude - startLatLng.longitude) < 0) {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_west));
                }
                else if((toLatLng.longitude - startLatLng.longitude) > 0) {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.motorbike_east));
                }
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toLatLng.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * toLatLng.latitude + (1 - t) * startLatLng.latitude;
                LatLng newPoint = new LatLng(lat, lng);
                marker.setPosition(newPoint);
                drawnLine.add(newPoint);
                polyline.setPoints(drawnLine);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
                else if(hide == true) {
                    marker.setVisible(false);
                    polyline.setVisible(false);
                }
            }
        });
    }
}
