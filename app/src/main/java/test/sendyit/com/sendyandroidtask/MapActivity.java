package test.sendyit.com.sendyandroidtask;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import test.sendyit.com.sendyandroidtask.handlers.Rider;

/**
 * Class representing the map activity
 */
public class MapActivity extends FragmentActivity {
    private final String TAG = "Sendy.MapActivity";

    private GoogleMap googleMap; // Might be null if Google Play services APK is not available.
    private ArrayList<Rider> riders;
    private TextView currTimeTV;

    /**
     * Called whenever the activity first starts
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        riders = new ArrayList<Rider>();
        setContentView(R.layout.activity_map);
        currTimeTV = (TextView)findViewById(R.id.curr_time_tv);
        initGoogleMap();
    }

    /**
     * Called whenever the activity resumes (was hidden by another activity but is now visible or after
     * onCreate is called)
     */
    @Override
    protected void onResume() {
        super.onResume();
        initGoogleMap();
    }

    /**
     * Sets up the map
     */
    private void initGoogleMap() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (googleMap != null) {
                ShowMarkersTask task = new ShowMarkersTask();
                task.execute(0);
            }
        }
    }

    /**
     * An instance of @link AsyncTask that handles getting and simulating the rider data on the map
     *
     * @see android.os.AsyncTask
     */
    private class ShowMarkersTask extends AsyncTask<Integer, Integer, JSONArray> {
        /**
         * Code to be run asynchronously from the main UI thread. Do not interact with any UI elements
         * here
         *
         * @param integers Plasibo Integer, not used at all
         *
         * @return  JSONArray carrying all the rider data
         * @see org.json.JSONArray
         */
        @Override
        protected JSONArray doInBackground(Integer... integers) {
            String json = loadJSONFromAsset();
            try {
                JSONObject payload = new JSONObject(json);
                JSONArray riderObjects = payload.getJSONArray("values");
                return riderObjects;

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Code run after the asynchronous part of the Tasker has finished running
         *
         * @param riderObjects  JSONArray carrying all the rider data. Might be null if no rider data
         *                      gotten
         *
         * @see org.json.JSONArray
         */
        @Override
        protected void onPostExecute(JSONArray riderObjects) {
            super.onPostExecute(riderObjects);
            if(riderObjects != null) {
                try {
                    //initialize the rider objects
                    riders = new ArrayList<Rider>();
                    Date now = new Date();
                    long startTime = -1;
                    long endTime = -1;
                    int firstRiderIndex = 0;
                    for(int index = 0; index < riderObjects.length(); index++) {
                        Rider currRider = new Rider(googleMap, riderObjects.getJSONObject(index));
                        if(startTime == -1 || currRider.getStartTime() < startTime) {
                            startTime = currRider.getStartTime();
                            firstRiderIndex = index;
                        }
                        if(endTime == -1 || currRider.getFinalTime() > endTime) {
                            endTime = currRider.getFinalTime();
                        }
                        riders.add(currRider);
                    }

                    //get the calibration time (time difference in milliseconds between now and the first point in the data)
                    final long calibrationTime = new Date().getTime() - startTime;
                    updateCurrTimeTV(calibrationTime);
                    final Handler handler = new Handler();
                    final int every = 1000;//1 second

                    //start showing the riders on the map
                    riders.get(firstRiderIndex).show();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(riders.get(firstRiderIndex).getCurrentLocation(), 9));
                    Runnable recurrsiveCode = new Runnable() {
                        @Override
                        public void run() {
                            updateCurrTimeTV(calibrationTime);
                            //animate movement for each of the riders
                            for(int index = 0; index < riders.size(); index++) {
                                //make sure the calibrated time is set on each of the riders before you start animating
                                if(riders.get(index).getCalibrationTime() == -1) {
                                    riders.get(index).setCalibrationTime(calibrationTime);
                                }
                                riders.get(index).animateToTime();
                            }
                            handler.postDelayed(this, every);
                        }
                    };

                    if(new Date().getTime() <= (endTime + calibrationTime)) {//only rerun the code if we still have points to show
                        handler.postDelayed(recurrsiveCode, every);
                    }
                    else {
                        Toast.makeText(MapActivity.this, R.string.info_simulation_finished, Toast.LENGTH_LONG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(MapActivity.this, R.string.warn_rider_data_unavailable, Toast.LENGTH_LONG);
            }
        }

        /**
         * Gets the string stored in the asset file containing the rider data
         *
         * @return  The string stored in the asset file storing the rider data
         */
        public String loadJSONFromAsset() {
            String json = null;
            try {
                InputStream is = MapActivity.this.getAssets().open("Rider_map_data_for_the_past_20_mins.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
            return json;
        }

        /**
         * Updates the @link TextView showing the current time being simulated
         *
         * @param calibration   The time difference in milliseconds between when the simulation started
         *                      and the first data point in the rider data being simulated
         *
         * @see android.widget.TextView
         */
        private void updateCurrTimeTV(long calibration) {
            currTimeTV.setText(getResources().getText(R.string.simulating) + " " + new Date(new Date().getTime() - calibration).toString());
        }
    }

}
