# Sendy Competence Task

Competence task for Sendy's Android Developer job. Points in the task are:

 - Map the 5 initial rider positions using markers on google map.
 - Using the positions array given generate a polyline to determine the riders path during the journey.
 - Using the positions and the time at each locations animate the markers along the polylines to show the rider movement in realtime (make sure the rider is at position X at time X given above).


You need to sign the app for it to be allowed to use some of the external APIs it needs. If you don't already have a release key, generate one by running:

    cd ~/.android
    keytool -genkey -v -keystore release.keystore -alias androidreleasekey -keyalg RSA -keysize 2048 -validity 10000

You can show the release key's SHA1 fingerprint by running:

    cd ~/.android
    keytool -v -list -keystore release.keystore

Add the following lines to the local.properties file in the project's root directory (you might have to create it):

    STORE_FILE=/home/[username]/.android/release.keystore
    STORE_PASSWORD=your_key_store_pw
    KEY_ALIAS=androidreleasekey
    KEY_PASSWORD=your_release_key_pw

Make sure your Android SDK has the following installed:

 - play-services:7.5.0
 - appcompact v7:22.2.0

This repository holds a Gradle Build System project. To run the project run:

```
./gradlew clean
./gradlew aR
```
## Download

You can download the signed Android apk from [here]( https://bitbucket.org/jasonrogena/sendy_simulation/raw/master/app/build/outputs/apk/app-release.apk).
